package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart implements ShoppingCartOperation {

    List<CartItem> cart = new ArrayList<CartItem>();

    public boolean addProducts(String productName, int price, int quantity) {
        if(quantity<=0 || price<0 || cart.size()>=PRODUCTS_LIMIT){
            return false;
        }
        if(cart.size()==0){
            cart.add(new CartItem(productName,price,quantity));
            return true;
        }
            for (CartItem item : cart) {
                if(item.getProductName().equals(productName)){
                    if(item.getPrice()!=price){
                        return false;
                    }
                    item.setQuantity(item.getQuantity()+quantity);
                    break;
                }
                else{
                    cart.add(new CartItem(productName,price,quantity));
                    break;
                }
            }

        return true;
    }

    public boolean deleteProducts(String productName, int quantity) {
        for(CartItem item : cart) {
            if (item.getProductName().equals(productName)) {
                if(item.getQuantity()>=quantity){
                    item.setQuantity(item.getQuantity()-quantity);
                }
                else {
                    return false;
                }
                if(item.getQuantity()==0){
                    cart.remove(item);
                }
                return true;
            }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        for(CartItem item : cart) {
            if(item.getProductName().equals(productName)){
                return item.getQuantity();
            }
        }
        return 0;
    }

    public int getSumProductsPrices() {
        int sum=0;
        for(CartItem item : cart) {
            sum+=item.getPrice();
        }
        return sum;
    }

    public int getProductPrice(String productName) {
        for(CartItem item : cart) {
            if(item.getProductName().equals(productName)){
                return item.getPrice();
            }
        }
        return 0;
    }

    public List<String> getProductsNames() {
        List<String> names = new ArrayList<>();
        for(CartItem item : cart) {
            names.add(item.getProductName());
        }
        return names;
    }
}
